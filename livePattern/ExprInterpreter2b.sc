// Eval in subclasses
sealed abstract class Expr {
  def eval: Int
}

case class Number(n: Int) extends Expr {
  override def eval = n
}

case class Sum(e1: Expr, e2: Expr) extends Expr {
  override def eval = e1.eval + e2.eval
}

def show(e: Expr): String = e match {
  case Number(n) => n.toString
  case Sum(e1, e2) => show(e1) + "+" + show(e2)
}

Sum(Number(3), Number(4)).eval
Sum(Number(3), Sum(Number(2), Number(4))).eval

show(Sum(Number(3), Number(4)))
show(Sum(Number(3), Sum(Number(2), Number(4))))