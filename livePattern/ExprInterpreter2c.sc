// Eval in superclass with matching
sealed abstract class Expr {
  def eval: Int = this match {
    case Number(n) => n
    case Sum(e1, e2) => e1.eval + e2.eval
  }
}

case class Number(n: Int) extends Expr

case class Sum(e1: Expr, e2: Expr) extends Expr

def show(e: Expr): String = e match {
  case Number(n) => n.toString
  case Sum(e1, e2) => show(e1) + "+" + show(e2)
}

Number(3).eval
Sum(Number(3), Number(4)).eval
Sum(Number(3), Sum(Number(2), Number(4)))


// Beware, this one should not be executed as REPL but plain mode (evaluation order)