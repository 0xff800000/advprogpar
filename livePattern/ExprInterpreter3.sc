// Adding product and show with parentheses
// This is for the exercises, not used during the course
sealed abstract class Expr
case class Number(n: Int) extends Expr
case class Sum(e1: Expr, e2: Expr) extends Expr
case class Product(e1: Expr, e2: Expr) extends Expr

def eval(e: Expr): Int = e match {
  case Number(n) => n
  case Sum(e1, e2) => eval(e1) + eval(e2)
  case Product(e1, e2) => eval(e1) * eval(e2)
}

def show(e: Expr): String = e match {
  case Number(n) => n.toString
  case Sum(e1, e2) => show(e1) + "+" + show(e2)
  case Product(e1, Sum(e2, e3)) => show(e1) + "*(" + show(Sum(e2, e3)) + ")"
  case Product(Sum(e1, e2), e3) => s"(${show(Sum(e1, e2))})*${show(e3)}" // With string interpolation
  case Product(e1, e2) => show(e1) + "*" + show(e2) // Default case
}

// Functionality checks
val expr1 = Sum(Product(Number(2), Number(3)), Number(4))
eval(expr1)
show(expr1)
assert(eval(expr1) == 10)

val expr2 = Product(Sum(Number(2), Number(3)), Number(4))
eval(expr2)
show(expr2)
assert(eval(expr2) == 20)

val expr3 = Product(Number(2), Sum(Number(3), Number(4)))
eval(expr3)
show(expr3)
assert(eval(expr3) == 14)

val expr0 = Product(Number(4), Number(12))
show(expr0)
eval(expr0)
assert(eval(expr0) == 48)