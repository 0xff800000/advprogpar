def patFoo(x: Any): Boolean = {
  x match {
    case x: Int if (x % 4 == 0) => true
    case x: Char => x >= 'A' && x <= 'Z'
    case x: Boolean => true
    case _ => false
  }
}

def foo(x: Int) = {
  x match {
    case 1 => "one"
    case 2 => "two"
    case 3 | 4 => "many"
    case _ => "other value"
  }
}

def bar(x: Any) = {
  x match {
    case 1 => "one"
    case 5.0 => "5 double"
    case "two" => 2
    case 'c' => "Letter c"
    case _ => "something else"
  }
}

def typeMatch(x: Any) = {
  x match {
    case a: Int => "Got an int, " + a
    case b: String => "The string " + b
    case _ => "Don't know what it was"
  }
}

def guardMatch(x: Any) = {
  x match {
    case a: Int if (a % 2 != 0) => "Odd int, " + a
    case b: String if (b.length > 4) => b + " is long"
    case b: String if (b.length <= 4) => b + " is short"
    case _ => "Don't know what it was"
  }
}

case class Foo(a: Int, b: String)

def extractMatch(q: Foo) = {
  q match {
    case Foo(x, y) => println("The int value was " + x + " and the string " + y)
  }
}

foo(3)
bar(1)
bar("two")
bar(9)

guardMatch(3)
guardMatch(2)
guardMatch("Hey")
guardMatch("Hello")