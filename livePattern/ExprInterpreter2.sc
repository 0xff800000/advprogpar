// Adding show method
abstract class Expr
case class Number(n: Int) extends Expr
case class Sum(e1: Expr, e2: Expr) extends Expr

def eval(e: Expr): Int = e match {
  case Number(n) => n
  case Sum(e1, e2) => eval(e1) + eval(e2)
}

def show(e: Expr): String = e match {
  case Number(n) => n.toString
  case Sum(e1, e2) => show(e1) + "+" + show(e2)
}

eval(Sum(Number(3), Number(4)))
eval(Sum(Number(3), Sum(Number(2), Number(4))))

show(Sum(Number(3), Number(4)))
show(Sum(Number(3), Sum(Number(2), Number(4))))