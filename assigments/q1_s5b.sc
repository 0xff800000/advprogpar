
def isPrime(i: Int) : Boolean = {
  i match {
    case i if i <= 1 => false
    case 2 => false
    case _ => !(2 to (i-1)).exists(x => i % x == 0)
  }
}

def primeSum(max : Int) : List[(Int, Int)] = {
  for {
    i <- (1 to max).toList
    j <- (1 to max).toList
    if(isPrime(i+j))
  } yield (i,j)
}

// ######## Question 1a
def removeDouble(elem : (Int, Int), l : List[(Int, Int)]) : List[(Int,Int)] = {
  if(l == Nil) Nil
  else if(elem == l.head || elem == l.head.swap) removeDouble(elem,l.tail)
  else l.head::removeDouble(elem,l.tail)
}

def removeDouble_match(l : List[(Int, Int)]) : List[(Int, Int)] = {
  l match {
    case Nil => Nil
    case a::y => {
      a :: removeDouble_match(removeDouble(a, y))
    }
  }
}

primeSum(5)
removeDouble_match(primeSum(5))

// ######## Question 1b
def removeDouble_fold(l : List[(Int, Int)]) : List[(Int, Int)] = {
  l.foldLeft(l)((a:List[(Int, Int)],b:(Int, Int)) => removeDouble(b,a):+b.swap)
}

removeDouble_fold(primeSum(5))

