def primeStream() : Stream[Int] = {
  val start = Stream.from(2)
  def iter(base : Stream[Int]): Stream[Int] = {
    base.head #:: iter(base.tail.filter((n:Int) => if(n % base.head != 0) true else false))
  }
  iter(start)
}

primeStream().take(7).toList