def fib(a : Int, b : Int) : Stream[Int] = {
  a #:: fib(b,b+a)
}

fib(1,1).take(7).toList
