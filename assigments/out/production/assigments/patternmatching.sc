def patFoo(x : Any) =
  x match{
    case a : Int if (a % 4 == 0) => true
    case a : Char  if(a.isUpper) => true
    case a : Boolean => true
    case _ => false
  }


patFoo(4)
patFoo(5)
patFoo('A')
patFoo('a')
patFoo(true)
patFoo()