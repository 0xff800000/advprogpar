def fact(x : Int) : Int ={
  def tfact(x : Int, p : Int) : Int = if(x==0) p else tfact(x-1, x*p)
  tfact(x,1)
}

fact(5)