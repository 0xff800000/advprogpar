// Complexity : O(n)
def last(xs : List[Any]) : Any =
  xs match {
    case a::Nil => a
    case a::b => last(b)
    case _ => "FAIL"
  }

def init(xs : List[Any]) : List[Any] =
  xs match {
    case a::b::Nil => a::Nil
    case a::b => a::init(b)
    case _ => Nil
  }

// Complexity : O(n²)
def reverse(xs : List[Any]) : List[Any] =
  xs match {
    case a::Nil => a::Nil
    case a => last(a) :: reverse(init(a))
  }

// Complexity : O(x)
def concat(x : List[Any], y : List[Any]) : List[Any] =
  x match {
    case a::Nil => a::y
    case a => a.head::concat(a.tail,y)
  }

def take(x : List[Any], n : Int) : List[Any] =
  n match {
    case 0 => x.head::Nil
    case _ => x.head::take(x.tail,n-1)
  }

def drop(x : List[Any], n : Int) : List[Any] =
  n match {
    case a if(a >= x.length) => Nil
    case 0 => x.tail
    case _ => drop(x.tail,n-1)
  }

def apply(x : List[Any], n : Int) : Any =
  n match {
    case a if(a >= x.length) => Nil
    case 0 => x.head
    case _ => apply(x.tail,n-1)
  }

last(List(1,2,3,4,5,6))
init(List(1,2,3,4,5,6))
reverse(List(1,2,3,4,5,6))
concat(List(1,2,3,4,5,6),List(1,2,3,4,5,6))
take(List(1,2,3,4,5,6),4)
drop(List(1,2,3,4,5,6),3)
apply(List(1,2,3,4,5,6),3)