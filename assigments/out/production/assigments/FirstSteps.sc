// ======== Exercice d,e ========
// Slide 72
12 + 56
def length = 3
length * 5

// Slide 73
def sqr(x : Double) = x * x
sqr(3)
sqr(3+4)
sqr(sqr(3))
def sumOfSqrs(x : Double, y : Double) = sqr(x) + sqr(y)

// ======== Exercice f ========
def fourthPower(x : Double) = sqr(sqr(x))
fourthPower(2)

// ======== Exercice d ========
// The type is String
def bar(x : Int, y : Boolean) = "Hello"