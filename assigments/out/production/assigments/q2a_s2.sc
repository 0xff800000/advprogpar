def sum(f : Int => Double, a : Int, b : Int) : Double = {
  if(a > b) 0 else f(a) + sum(f, a+1, b)
}

def sumInts(a : Int, b : Int) : Double = sum(x => x, a, b)

def sumCubes(a : Int, b : Int) : Double = sum(x => x*x*x, a, b)


sumInts(3,5)
sumCubes(0,3)

def tsum(f: Double => Double, a : Int, b : Int) : Double = {
  def iter(a : Double, acc : Double) : Double = {
    if(a > b)  acc
    else iter(a+1, acc + f(a))
  }
  iter(a, 0)
}

tsum(x => x, 3,5)
tsum(x => x*x*x, 0,3)
