def any[Any](p : Any => Boolean)(l: List[Any]) : Boolean = {
  if(l.isEmpty) false
  else if(p(l.head)) true
  else any(p)(l.tail)
}

def every[Any](p : Any => Boolean)(l: List[Any]) : Boolean = {
  l match{
    case a::Nil => p(a)
    case a => p(a.head) && every(p)(a.tail)
  }
}


any[Int]((a:Int)=>if(a>5)true else false)(List(1,2,3,4,5,6))
any[Int]((a:Int)=>if(a>5)true else false)(List(1,2,3,4,5))
any[Int]((a:Int)=>if(a>5)true else false)(List(10,20,30,40,50))
every[Int]((a:Int)=>if(a>5)true else false)(List(1,2,3,4,5))