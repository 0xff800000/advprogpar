class Rational(n: Int, d: Int) {
  require(d != 0)
  val g = gcd(n,d)
  def num = n / g
  def denom = d / g

  private def gcd(x: Int, y: Int) : Int =
    if(y == 0) x else gcd(y,x % y)

  def +(that: Rational) : Rational =
    new Rational(num*that.denom+ that.num*denom, denom*that.denom)

  def -(that: Rational) : Rational =
    new Rational(num*that.denom - that.num*denom, denom*that.denom)

  def *(that: Rational) : Rational =
    new Rational(num*that.num, denom*that.denom)

  def *(x: Int) : Rational =
    new Rational(num*x, denom)

  def /(that: Rational) : Rational =
    new Rational(num*that.denom, denom*that.num)

  def ==(that: Rational) : Boolean =
    num * that.denom == denom * that.num

  def unary_-() : Rational =
    new Rational(0,1).-(this)

  def <(that: Rational) : Boolean =
    num * that.denom < denom * that.num

  def max(that: Rational) : Rational =
    if(this.<(that)) that else this


  override def toString: String = num + "/" + denom
}


object Rational{
  implicit def intToRat(x:Int) = new Rational(x,1)
}

val r1 = new Rational(1,3)
val r2 = new Rational(2,3)

// Operators are actually method calls
// x+y === x.+(y)


r1 + r2 * r1
r1 + (r2 * r1)
2 * new Rational(1,3)