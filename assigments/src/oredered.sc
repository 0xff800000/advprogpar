case class Foo(x:Int) extends Ordered[Foo]{
	def compare(that:Foo) : Int = {
		x - that.x
	}
}

val a = Foo(2)
val b = Foo(3)
val c = Foo(2)

a < b
a > b
b <= a

a == b
a == c

List(Foo(4), Foo(5), Foo(-1), Foo(9)).sortWith(_<_) 
