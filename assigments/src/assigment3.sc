
abstract class IntSet() {
  def add(x: Int): IntSet
  def contains(x: Int): Boolean
  override def toString: String
  def foreach(f: Int => Unit): Unit
  def print : String
  def addUnique(x : Int): IntSet =
    if(!this.contains(x)) this.add(x) else this
  def getElem() : List[Int]
}

class NonEmpty(elem: Int, left: IntSet, right: IntSet) extends IntSet{
  override def add(x: Int): IntSet = {
    if(x < elem) new NonEmpty(elem, left add x, right)
    else if(x > elem) new NonEmpty(elem, left, right add x)
    else this
  }

  override def contains(x: Int): Boolean = {
    if(x < elem) left contains x
    else if(x > elem) right contains x
    else true
  }

  def foreach(f: Int => Unit) : Unit = {
    f(elem)
    left.foreach(f)
    right.foreach(f)
  }

  override def toString: String = elem.toString
  def print = "(" + left.print + "|" + elem + "|" + right.print + ")"
  override def getElem() : List[Int] = {
    //elem::left.getElem()::right.getElem()
    elem::left.getElem()::Nil
  }

}

object Empty extends IntSet() {
  override def contains(x: Int): Boolean = false
  override def add(x: Int): IntSet = new NonEmpty(x, Empty, Empty)
  override def toString: String = "-"
  def foreach(f: Int => Unit) : Unit = {
  }
  def print = "-"

  override def getElem(): List[Int] = Nil

}

def println(x: IntSet) : Unit =
  printf(x.print)


new NonEmpty(3,Empty,Empty).toString

println(Empty)
println(Empty.add(3))
println(Empty.add(3).add(2))

def println2(x: Int) : Unit =
  printf(x.toString() + " ")

def union(x : IntSet, y : IntSet) : IntSet = {
  if(!x.isInstanceOf[NonEmpty]) y
  else if(!y.isInstanceOf[NonEmpty]) x
  else Empty
}

(Empty.add(3).add(2).add(7)).foreach(println2)

