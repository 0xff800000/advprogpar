import scala.annotation.tailrec

// 1)
def fib(x : Int) : Int = {
  if(x == 0) 0 else if(x == 1) 1 else fib(x-1) + fib(x-2)
}

fib(10)

// 2)
def fib(x : Int) : Int = {
  @tailrec def fibIter(x : Int, x1: Int, x2 : Int) : Int = {
    if(x == 0) x+x1
    else fibIter(x-1, x1+x2, x1)
  }
  fibIter(x,0,1)
}

fib(10)
// 0 1 1 2 3 5 8 13
