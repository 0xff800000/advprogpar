def filter(f: (Int) => Boolean, l : List[Int]) : List[Int] = {
  l match {
    case Nil => l
    case _ => if(f(l.head)) l.head::filter(f,l.tail)
    else filter(f,l.tail)
  }
}

def filterFold[A](f: A => Boolean, l : List[A]) : List[A] = {
  (l foldRight List.empty[A]) ((x : A, y:List[A]) => if(f(x)) x::y else y)
}

val b = (1 to 10).toList

filter(_ % 2 == 0, b)
filterFold((x:Int) => x % 2 == 0, b)

def partition(f : (Int) => Boolean, l : List[Int]) : (List[Int],List[Int]) = {
  val a1 = filter(f,l)
  val a2 = filter((x:Int) => if(f(x)) false else true,l)
  (a1,a2)
}

partition(_ % 2 == 0, b)


case class User(name: String, age:Int)

val userBase = List(
  User("Travis",28),
  User("Kelly",33),
  User("Jennifer",44),
  User("Dennis",23),
)

val names = for(p <- userBase if p.age < 30) yield p.name;

