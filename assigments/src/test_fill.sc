def append(l : List[Any], elem : Any) : List[Any] = {
  l :+ elem
}

//List(1,2) map ((x : List[Any]) => x::x)

append(List(1,2),"LOL")
//List(1,2) map (append)

def fill(elem : Any, n : Int) : List[Any] = {
  def fill_iter(acc : List[Any], i : Int) : List[Any] = {
    if(i == 0) acc
    else fill_iter(acc :+ elem, i-1)
  }
  if(n <= 0) List.empty[Any]
  else fill_iter(List(elem),n-1)
}

fill("LOL",10)