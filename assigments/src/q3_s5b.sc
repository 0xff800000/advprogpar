def inCheck(q1: (Int, Int), q2: (Int, Int)) = {
  q1._1 == q2._1 ||   q1._2 == q2._2 || (q1._1 - q2._1).abs == (q1._2 - q2._2).abs
}

def isSafe(queen: (Int, Int), queens: List[(Int, Int)]) =
  queens forall (q => !inCheck(queen, q))

def queens(n : Int) : List[List[(Int, Int)]] = {
  def placeQueens(k : Int) : List[List[(Int, Int)]] =
    if(k == 0)
      List(List())
    else
      for {
        queens <- placeQueens(k-1)
        column <- 1 to n
        queen = (k, column)
        if isSafe(queen, queens)
      } yield queen :: queens
  placeQueens(n)
}
/*
def printSolution(solutions: List[(Int, Int)]) = {
  val str =
    for {
      y <- 1 to solutions.length
    } yield
      for {
        x <- 1 to solutions.length
      } yield ({
        if (solutions.contains((x, y)))
          "\u265b"
        else
          "_"
      }) .foldLeft("")((a,b) => "|"+a+b) + "|"
  str.foldLeft("")((a,b) => a+"\n"+b).toString
}
}*/

def printSolution(solutions: List[(Int, Int)]) = {
  val str =
    for {
      y <- 1 to solutions.length
    } yield(
      for {
        x <- 1 to solutions.length
      } yield {
        if (solutions.contains((x, y)))
          "\u265b"
        else
          "_"
      }) .foldLeft("")((a,b) => a+"|"+b) + "|"
  str.foldLeft("")((a,b) => a+"\n"+b)
}

def printChessBoard(sol : List[List[(Int, Int)]]) : String = {
  if(sol == Nil) "No solution"
  else{
    val solution = (for {
      s <- sol
    } yield printSolution(s) + "\n======= \n")
    val index = (for{
      i <- 1 to sol.length
    } yield "Solution " + i + ":" + solution(i-1)).foldLeft("")((a,b) => a+b)
    index
  }
}

println(printChessBoard(queens(8)))
