def isPrime(a: Int) : Boolean = {
  if(a >= 1 && a <= 3) true
  else if (a < 1) false
  else
    (3 to a-1).toList.foldLeft(true)(
      (b:Boolean, i:Int) => if(b && (a % i != 0)) true else false
    )
}

def factors(n: Int): Stream[Int] = {
  val primes = 2 #:: Stream.from(3,2).filter(isPrime)
  def primeIter(nb: Int, pr: Stream[Int]): Stream[Int] = {
    if(pr.head > nb) Stream.empty[Int]
    if(nb == 0 || nb == 1) Stream.empty[Int]
    else {
      if (nb % pr.head == 0) pr.head#::primeIter(nb / pr.head, pr)
      else primeIter(nb, pr.tail)
    }
  }
  //primes.tail.tail
  primeIter(n,primes)
}

factors(90).toList
