// Exercice 1c
val xs = List(1,3,4,99);
val ys = List(3,4,5,6);

xs.zip(xs.indices).filter(q => ys.contains(q._1)).map(p => p._2);
for(x <- xs.zip(xs.indices) if ys.contains(x._1)) yield x._2

// Exercice 1e
val l = List(1,2,3,4)
def concat(b: Any, a: Any) : String = b + "-" + a
l reduceLeft (concat)

// Execice 2a
def toUpper(s : String) : String = {
  def upperIter(cs : List[Char],acc : String) : String = {
    if(cs == List.empty[Char])
      acc
    else
      upperIter(cs.tail,acc+cs.head.toUpper)
  }
  upperIter(s.toList, "")
}

toUpper("aAbBcCdD")

// Exercice 2b
def balanceMatch(chars : List[Char]) : Boolean = {
  def iter(cs : List[Char], num : Int) : Int = {
    if (cs == List.empty[Char]) num
    else
      cs.head match {
        case '(' => iter(cs.tail, num + 1)
        case ')' => iter(cs.tail, num - 1)
        case _ => iter(cs.tail, num)
      }
  }
  iter(chars, 0) == 0
}

balanceMatch("(if (x==0) then max (1,x))".toList)
balanceMatch("(if (x==0) then max (1,x))))))".toList)

// Exercice 3a
def countTrue(l : List[Boolean]) : Int = {
  l.foldLeft(0)((sum: Int, a:Boolean) => (if(a) sum+1 else sum))
}

countTrue(List(true,false,true,true))

// Exercice 2d1
case class Customer(name : String, id: Int, orders: List[Item])
case class Item(name: String, qty: Int, price: Int)

val o1 = Item("Milk", 1, 1); val o2 = Item("Pizza", 2, 5);
val o3 = Item("Coke", 1, 3); val o4 = Item("Beer", 6, 3);

val c1 = Customer("John Doe", 123, List(o1, o2))
val c2 = Customer("Homer Simpson", 124, List(o1, o3, o4))
val c3 = Customer("Marge Simpson", 125, List(o4, o4))

def check(cust : Customer) : Int = {
  cust.orders.foldLeft(0)( (sum: Int, i: Item) => sum+(i.qty*i.price) )
}

check(c1)
check(c2)

// Exercice 2d2
def getItems(l: List[Customer]) : List[(Item,Int)] = {
  def unifyLists(lc: List[Customer]) : List[Item] = {
    lc.foldLeft(List.empty[Item])( (li: List[Item], c: Customer) => li:::c.orders )
  }
  def countDistinct(dis: List[Item], ords: List[Item]) : List[Int] = {
    dis.map(
      (ui: Item) => ords.foldLeft(0)( (sum: Int, i:Item)=>if(i==ui) sum+1 else sum )
    )
  }
  unifyLists(l).distinct.zip(countDistinct(unifyLists(l).distinct,unifyLists(l)))
}

getItems(List(c1,c2,c3))

