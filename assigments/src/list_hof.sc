// Filter
(1 to 5).toList filter ((p:Int) => (p > 3))

// reduceLeft
(1 to 5).toList reduceLeft ((x:Int, y:Int) => (x + y))
(1 to 5).toList sum

(1 to 5).toList reduceLeft ((x:Int, y:Int) => (x min y))
(1 to 5).toList min

def verboseMax(x:Int, y:Int) = {
  val winner = x max y
  println(s"compared $x and $y, $winner was larger")
  winner
}

val l = List(1,4,12,7)
l reduceLeft (verboseMax)


// Reduce right
val abc = List("A", "B", "C")
def add(x:String, y:String) = x + y
abc reduceRight(add)

// foldRight
val l = List(1,2,3,4)
(l foldRight 0) ((x,y) => x+y)
(l foldRight 1) ((x,y) => x*y)

// scanLeft
val l = List(1,2,3,4,3)
(l scanLeft 0) ((x,y) => x+y)

def length[A](x: List[A]) : Int = {
  (x foldRight(0)) ((x:A, y:Int) => y + 1)
}

length(l)

def map[A, B](x: List[A], f:A=>B) : List[B] = {
  (x foldRight List.empty[B]) ((x,y) => f(x)::y)
}

map(l,((x:Int)=>x*2))

def dup[A](x: List[A]) : List[A] = {
  (x foldRight List.empty[A]) ((x,y) => x::x::y)
}

dup(l)


// Merge
val n1 = List(1,2,4,6)
val n2 = List(2,3,5,7)

def merge(x: List[Int], y: List[Int]) : List[Int] = {
  (x,y) match
  {
    case (aList,Nil) => aList
    case (Nil,aList) => aList
    case (e1::rest1, e2::rest2) =>
      if(e1 <= e2) e1::merge(rest1,y) else e2::merge(x,rest2)
  }
}

merge(n1,n2)