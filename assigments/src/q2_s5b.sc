val cities = List("Paris", "London", "Berlin", "Lausanne")
val relatives = List("Grandma", "Grandpa", "Aunt Lottie", "Dad")
val travellers = List("Pierre-Andre", "Rachel")

for{
  i <- cities
  j <- relatives
  k <- travellers
  if(j.startsWith("G"))
} yield println("Dear %s, Wish .. here in %s, %s".format(j,i,k))

