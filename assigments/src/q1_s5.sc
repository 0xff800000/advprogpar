// ######## Question 1a
def lengthString(l : List[String]) : List[Int] = l map ((s: String) => s.length)
val l = List("How", "long", "are", "we?")
lengthString(l)

// ######## Question 1b
def myFill[A](elem : A, n : Int) : List[A] = {
  if(n > 0) elem::myFill(elem,n-1)
  else elem::Nil
}

def myFill_map[A](elem : A, n : Int) : List[A] = {
  if(n > 0) elem::myFill(elem,n-1)
  else elem::Nil
}

def createList[A](elem : A, n: Int) : List[A] = {
  if(n > 0) List[A]()::createList(elem, n-1)
  else List[A]()::Nil
  ???
}

def dup[A](l: A, n: Int): List[A] = {
  require(n >= 0)
  List.range(1, n + 1).map(x => l)
}

myFill("LOL", 4)
dup("LOL", 4)
myFill(List(1,2), 4)

// ######## Question 1c

def dot(a:List[Int], b:List[Int]): List[Int] = {
  a.zip(b).map((a: (Int,Int)) => a._1*a._2)
}

//List(1,2,3).zip(List(2,4,3)).map((a: (Int,Int)) => a._1*a._2)
dot(List(1,2,3),List(2,4,3))

// ######## Question 2a
def areTrue(l : List[Boolean]) : Boolean = {
  l.foldRight(true)((a:Boolean,b:Boolean) => if(a == b) true else false)
}

areTrue(List(true,true,false))
areTrue(List(true,true,true))

// ######## Question 2b
def lString(l : List[String]) : Int = {
  l.foldRight(0)((a:String,sum:Int)=>a.length+sum)
}

lString(List("Folding", "is", "fun"))

// ######## Question 2c
def longest(l : List[String]) : (Int,String) = {
  val v1 = l.map((a:String) => a.length).zip(l) // create list of (str.length, str)
  v1.foldLeft((0,""))( (a:(Int,String),b:(Int,String)) => if(a._1 > b._1) a else b)
}

longest(List("What", "is", "the", "Longest?"))

// ######## Question 2d
def isPresent[A](l : List[A],elem : A) : Boolean = {
  l.foldRight(Nil.asInstanceOf[A])((a:A,b:A)=>if(a == elem) a else b) == elem
}

isPresent(List(1,2,3,4),5)
isPresent(List(1,2,3,4),3)

// ######## Question 2e
/*def flattenList[A,B](l : List[B]) : List[A] = {
  //l.foldRight(Nil.asInstanceOf[A])((a:A,b:B) => List(a)::List(b)::Nil.asInstanceOf[A])
  println(l.toString())
  val bitch = l.foldRight(Nil.asInstanceOf[A])((a:A,b:B) => a :+ b)
  println(bitch.toString)
  l
  ???
}*/


def myflatten(xs: List[Any]): List[Any] =
  xs match {
    case List() => List()
    case y::ys => y match {
      case k::ks => myflatten(List(k)) ::: myflatten(ks) ::: myflatten(ys)
      case _ => y :: myflatten(ys)
    }
  }

//val FUCKOFF = List(List(1,2,3)::List(4)::List(5)::Nil::List(6))
val FUCKOFF = List(List(1,2,3),List(4),List(5),List(6))
List(1,2,3,4) :+ List(5)
//flattenList(FUCKOFF)
myflatten(FUCKOFF)
