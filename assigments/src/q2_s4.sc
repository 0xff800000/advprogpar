sealed abstract class BinaryTree
case class Leaf(value : Int) extends BinaryTree
case class Node(left: BinaryTree, right: BinaryTree) extends BinaryTree

def sum(bt : BinaryTree) : Int =
  bt match{
    case Node(l,r) => sum(l) + sum(r)
    case Leaf(v) => v
  }

def min(bt : BinaryTree) : Int =
  bt match {
    case Node(l,r) => if(min(l) < min(r)) min(l) else min(r)
    case Leaf(v) => v
  }

sum(Node(Node(Leaf(1),Leaf(2)),Node(Leaf(3),Leaf(4))))
min(Node(Node(Leaf(1),Leaf(2)),Node(Leaf(-3),Leaf(4))))