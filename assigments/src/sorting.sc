/*
def insert(x: Int, l : List[Int]) : List[Int] =
  if(l.isEmpty) x::Nil
  else if(x < l.head) x::l
  else l.head::insert(x,l.tail)
*/
def insert(elem : Int, l : List[Int]) : List[Int] = {
  l match {
    case Nil => elem::Nil
    case x::y => {
      if(elem < x)
        elem::l
      else
        x::insert(elem, y)
    }
  }
}
def isort(xs : List[Int]) : List[Int] =
  if(xs.isEmpty) Nil
  else insert(xs.head, isort(xs.tail))

insert(3, Nil)
insert(3,4::Nil)
insert(3,2::Nil)
isort(List(7,3,9,2))