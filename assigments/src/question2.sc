def abs(x : Double) : Double = if(x < 0) -x else x

def isGoodEnough(x : Double, Xapprox : Double) : Boolean =
  if(abs(x - (Xapprox*Xapprox)) < 0.0001) true else false

def improve(x : Double, approx : Double) : Double =
  approx - ((approx * approx)-x)/(2*approx)

def sqrtn(x : Double, approx : Double) : Double =
  if(isGoodEnough(x,approx)) approx
  else sqrtn(x,improve(x, approx))

def sqrt(x : Double) : Double =
  sqrtn(x, 1.0)

// improve(4.0,improve(4.0,1.0))

sqrt(4.0)
sqrt(2)

