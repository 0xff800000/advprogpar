import language.implicitConversions

abstract sealed class Temperature(tmp : Double) {
  val temp : Double = tmp
}

case class Celsius(tmp : Double) extends Temperature(tmp) {
  override def toString = temp + "°C"
}

case class Kelvin(tmp : Double)  extends Temperature(tmp) {
  override def toString = temp + "K"
}

implicit def c2k(a : Celsius) : Kelvin = Kelvin(a.temp + 271.15)
implicit def k2c(a : Kelvin) : Celsius = Celsius(a.temp - 271.15)
implicit def double2c(a : Double) : Celsius = Celsius(a)
implicit def double2k(a : Double) : Kelvin = Kelvin(a)

val a : Celsius = 30
val b : Kelvin = 30
val c : Kelvin = Celsius(10)
val d : Celsius = c
val e : Temperature = d

println(a) // should print 30°C
println(b) // should print 30 K

