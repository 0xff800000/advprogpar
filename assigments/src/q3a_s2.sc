def prod(f: Double => Double, a : Int, b : Int) : Double = {
  def iter(a : Double, acc : Double) : Double = {
    if(a > b)  acc
    else iter(a+1, acc * f(a))
  }
  iter(a, 1)
}

def sum(f : Int => Double, a : Int, b : Int) : Double = {
  if(a > b) 0 else f(a) + sum(f, a+1, b)
}

def factorial (x : Int)= prod(b => b,1,x)

def general(f: Double => Double, op: (Double,Double) => Double, a: Int, b: Int, startAcc: Int): Double ={
  def iter(a : Double, acc : Double) : Double = {
    if(a > b)  acc
    //else iter(a+1, op(acc)(f(a)))
    else iter(a+1, op(acc,f(a)))
  }
  iter(a, startAcc)
}

def gsum(f: Double => Double, a: Int, b: Int) = general(f, (x,y)=>x+y, a,b,0)

def gprod(f: Double => Double, a: Int, b: Int) = general(f, (x,y)=>x*y, a,b,1)

prod(x => x, 1,5)
factorial(5)


gsum(x=>x*x, 4,9)
sum(x=>x*x, 4,9)
prod(x => x*x*x, 2,4)
gprod(x => x*x*x, 2,4)