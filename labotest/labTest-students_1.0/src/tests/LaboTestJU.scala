package tests

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

/**
  * *******************************************************
  *  _____       _       _       ____       ____
  * |_   _|     / \   __| __   _|  _ \ _ __|  _ \ __ _
  *   | |_____ / _ \ / _` \ \ / | |_) | '__| |_) / _` |
  *   | |_____/ ___ | (_| |\ V /|  __/| |  |  __| (_| |
  *   |_|    /_/   \_\__,_| \_/ |_|   |_|  |_|   \__,_|
  *
  * *******************************************************
  *
  * ScalaUnit test file for test lab
  *
  * @author pmudry
  * @version 1.0
  */

@RunWith(classOf[JUnitRunner])
class LaboTestJU extends FlatSpec {

  /**
    * Test suite for question 1
    */

  import exercises.Exercise1._

  "Ex1 : dup" should "produce the appropriate list result" in {
    assert(dup(List(3, 2, 0), List(1, 2, 3)) == List(1, 1, 1, 2, 2))
    assert(dup(List(1, 1, 1), List(1, 2, 3)) == List(1, 2, 3))
    assert(dup(List(0, 0, 0), List(1, 2, 3)) == List())
  }

  it should "also work for other lists" in {
    assert(dup(List(3, 2, 0), List(2, 3, 1)) == List(2, 2, 2, 3, 3))
    assert(dup(List(1, 1, 1), List(2, 3, 1)) == List(2, 3, 1))
    assert(dup(List(0, 0, 0), List(2, 3, 1)) == List())
  }

  "Ex1 : removeDup" should "produce the appropriate list" in {
    assert(removeDup(List(1, 2, 3, 3)) == List(1, 2, 3))
    assert(removeDup(List(1, 2, 1, 3)) == List(1, 2, 3))
    assert(removeDup(List(1, 2, 2, 2, 3)) == List(1, 2, 3))
    assert(removeDup(List(1, 1)) == List(1))
  }

  it should "also work if element is not duplicated" in {
    assert(removeDup(List(1, 2, 3)) == List(1, 2, 3))
    assert(removeDup(List('a', 'b', 'c')) == List('a', 'b', 'c'))
  }

  "Ex1 : zip" should "zip two simple lists together" in {
    assert(zip(List(1, 2, 3), List(4, 5, 6)) == List((1, 4), (2, 5), (3, 6)))
    assert(zip(List('a', 'b', 'c'), List('d', 'e', 'f')) == List(('a', 'd'), ('b', 'e'), ('c', 'f')))
  }

  it should "also work for empty lists" in {
    assert(zip(List(1, 2, 3), Nil) == Nil)
    assert(zip(Nil, List(1, 2, 3)) == Nil)
    assert(zip(List("hello", "world"), List.empty[String]) == List.empty[String])
  }

  "Ex1 : zipWith" should "apply the function between two lists" in {
    assert(zipWith(List(1, 2, 3), List(4, 5, 6))((a, b) => a + b) == List(5, 7, 9))
    assert(zipWith(List(1, 2, 3), List(4, 5, 6))((a, b) => a - b) == List(-3, -3, -3))
  }

  /**
    * Exercise 2
    */
  import exercises.Exercise2._

  "Ex2 : password generator" should "generates all passwords" in {
    def check(a: List[String], b: List[String]) = {
      assert(a forall (b contains))
      assert(b forall (a contains))
    }

    val r1 = List("a", "b", "c")
    val c1 = gen("abc", 1)
    check(r1, c1)

    val r2 = List("aa", "ab", "ac", "ba", "bb", "bc", "ca", "cb", "cc")
    val c2 = gen("abc", 2)
    check(r2, c2)

    val r3 = List("aaa", "aab", "aac", "aba", "abb", "abc", "aca", "acb", "acc",
      "baa", "bab", "bac", "bba", "bbb", "bbc", "bca", "bcb", "bcc", "caa",
      "cab", "cac", "cba", "cbb", "cbc", "cca", "ccb", "ccc")
    val c3 = gen("abc", 3)
    check(r3, c3)

    val r4 = List("aaaa", "aaab", "aaac", "aaba", "aabb", "aabc", "aaca", "aacb", "aacc",
      "abaa", "abab", "abac", "abba", "abbb", "abbc", "abca", "abcb", "abcc", "acaa",
      "acab", "acac", "acba", "acbb", "acbc", "acca", "accb", "accc", "baaa", "baab",
      "baac", "baba", "babb", "babc", "baca", "bacb", "bacc", "bbaa", "bbab", "bbac",
      "bbba", "bbbb", "bbbc", "bbca", "bbcb", "bbcc", "bcaa", "bcab", "bcac", "bcba",
      "bcbb", "bcbc", "bcca", "bccb", "bccc", "caaa", "caab", "caac", "caba", "cabb",
      "cabc", "caca", "cacb", "cacc", "cbaa", "cbab", "cbac", "cbba", "cbbb", "cbbc",
      "cbca", "cbcb", "cbcc", "ccaa", "ccab", "ccac", "ccba", "ccbb", "ccbc", "ccca",
      "cccb", "cccc")

    val c4 = gen("abc", 4)
    check(r4, c4)
  }

  /**
    * Test suite for question 3
    */

  import exercises.Exercise3._

  "Ex3 : symmetric" should "report correct values" in {
    val q1 = Node(Empty, 1, Empty)
    val q2 = Node(Node(Empty, 3, Empty), 1, Empty)
    val q3 = Node(Node(Empty, 3, Empty), 1, Node(Empty, 5, Empty))
    val q4 = Node(Node(Node(Empty, 2, Empty), 3, Node(Empty, 6, Empty)), 1, Node(Node(Empty, 10, Empty), 5, Node(Empty, 8, Empty)))

    assert(q1.isSymmetric)
    assert(!q2.isSymmetric)
    assert(q3.isSymmetric)
  }

  "Ex3 : breadth first" should "traverse the tree left to right, level by level" in {
    val b0 = Node(Empty, 1, Empty)
    val b1 = Node(Node(Node(Empty, 4, Empty), 2, Node(Empty, 5, Empty)), 1, Node(Node(Empty, 6, Empty), 3, Empty))
    val b2 = Node(Node(Empty, 2, Empty), 1, Node(Empty, 3, Empty))
    assert(b0.traverseBreadthFirst() == List(1))
    assert(b1.traverseBreadthFirst() == List(1,2,3,4,5,6))
    assert(b2.traverseBreadthFirst() == List(1,2,3))
  }
}