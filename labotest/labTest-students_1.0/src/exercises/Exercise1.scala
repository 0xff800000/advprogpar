package exercises

/**
  *
  *  _____       _       _       ____       ____
  * |_   _|     / \   __| __   _|  _ \ _ __|  _ \ __ _
  *   | |_____ / _ \ / _` \ \ / | |_) | '__| |_) / _` |
  *   | |_____/ ___ | (_| |\ V /|  __/| |  |  __| (_| |
  *   |_|    /_/   \_\__,_| \_/ |_|   |_|  |_|   \__,_|
  *
  * Exercise 1 of test lab
  *
  * @version 1.0
  * @author XXX
  */

object Exercise1 extends App {
  def dup[A](r: List[Int], l: List[A]): List[A] = {
    def fill(elem : (Int, A)) : List[A] = {
      List.range(1, elem._1 + 1).map(x => elem._2)
    }
    if(r == Nil) List.empty[A]
    else
      r.zip(l) map(fill) flatten
  }


  def removeDup[A](l: List[A]): List[A] = {
    l.foldLeft(List[A]()) { (list, item) =>
      val exists = list.find((t: A) => t == item)
      if (exists.isEmpty)
        item :: list
      else
        list
    }.reverse
  }

  def zip[A, B](first: List[A], second: List[B]): List[(A, B)] = {
    def pair_iter(p1: List[A], p2 : List[B], res : List[(A, B)]) : List[(A, B)] = {
      if(p1 == Nil || p2 == Nil) res
      else pair_iter(p1.tail, p2.tail, res:+ (p1.head, p2.head))
    }
    pair_iter(first,second,List.empty[(A, B)])
  }

  def zipWith[A, B, C](xs: List[A], ys: List[B])(f: (A, B) => C): List[C] = {
    def pair_iter(p1: List[A], p2 : List[B], res : List[C]) : List[C] = {
      if(p1 == Nil || p2 == Nil) res
      else pair_iter(p1.tail, p2.tail, res:+ f(p1.head, p2.head))
    }
    pair_iter(xs,ys,List.empty[C])
  }
}