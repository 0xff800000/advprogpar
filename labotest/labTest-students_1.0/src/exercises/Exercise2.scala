package exercises

/**
  ********************************************************
  *  _____       _       _       ____       ____
  * |_   _|     / \   __| __   _|  _ \ _ __|  _ \ __ _
  *   | |_____ / _ \ / _` \ \ / | |_) | '__| |_) / _` |
  *   | |_____/ ___ | (_| |\ V /|  __/| |  |  __| (_| |
  *   |_|    /_/   \_\__,_| \_/ |_|   |_|  |_|   \__,_|
  *
  ********************************************************
  *
  * Exercise 2 of test lab
  *
  * @version 1.0
  * @author XXX
  */

object Exercise2 extends App {
  def gen(charSet: String, length: Int): List[String] = {
    val passes = for {
      i <- 1 to length
    } yield for{
      c <- charSet.toList
    } yield c.toString()
    print(passes)
    passes.toList.flatten
  }
}
