package exercises


/**
  * *******************************************************
  *  _____       _       _       ____       ____
  * |_   _|     / \   __| __   _|  _ \ _ __|  _ \ __ _
  *   | |_____ / _ \ / _` \ \ / | |_) | '__| |_) / _` |
  *   | |_____/ ___ | (_| |\ V /|  __/| |  |  __| (_| |
  *   |_|    /_/   \_\__,_| \_/ |_|   |_|  |_|   \__,_|
  *
  * *******************************************************
  *
  * Exercise 3 of test lab
  *
  * @version 1.0
  * @author XXX
  */

object Exercise3 extends App {
  sealed abstract class Tree {
    def isMirrorOf(other: Tree): Boolean
    def isSymmetric(): Boolean
    def isEmpty() : Boolean
    def traverseBreadthFirst(): List[Int] = {
      def compute_depth(t : Tree, acc: Int) : Int = {
        if(t.isEmpty()) acc
        else compute_depth(t.asInstanceOf[Node].left,acc+1) max compute_depth(t.asInstanceOf[Node].right,acc+1)
      }
      def traverse_Breath(tree : Tree, height : Int) : List[Int] = {
        if(tree.isEmpty()) List.empty[Int]
        else if(compute_depth(tree,0) == height) List(tree.asInstanceOf[Node].elem)
        else traverse_Breath(tree.asInstanceOf[Node].left,height) ::: traverse_Breath(tree.asInstanceOf[Node].right,height)
      }
      print(traverse_Breath(this,2))
      List(123) //DEBUG
      /*
      //Marche pas
      def bfs(t : Tree, acc : List[Int]) : List[Int] = {
        if(t.isEmpty()) acc
        else bfs(t.asInstanceOf[Node].right, acc:+this.right.elem:+this.left.elem)
      }
      if(this.isEmpty()) List.empty[Int]
      else bfs(this, List.empty[Int])*/
    }
  }

  case class Node(left: Tree, elem: Int, right: Tree) extends Tree {
    def isMirrorOf(other: Tree): Boolean = {
      def isMirrorof_iter(tr : Tree, tl : Tree) : Boolean = {
        (tr,tl) match {
          case (Node(a,b,c),Node(a1,b1,c1)) =>
            isMirrorof_iter(tr.asInstanceOf[Node].right,tr.asInstanceOf[Node].left) && isMirrorof_iter(tr.asInstanceOf[Node].left,tr.asInstanceOf[Node].right)

          case (Empty,Empty) => true
          case _ => false
        }
      }
      (left,right) match {
        case (Node(a,b,c),Empty) => false
        case (Empty,Node(a,b,c)) => false
        case _ => isMirrorof_iter(left,right)
      }

    }
    def isSymmetric: Boolean = {
      this.right.isMirrorOf(left)
    }
    def isEmpty: Boolean = {
      false
    }
  }

  case object Empty extends Tree {
    def isMirrorOf(other: Tree): Boolean = {
      other match {
        case Empty => true
        case _ => false
      }
    }

    def isSymmetric: Boolean = true

    def isEmpty: Boolean = {
      true
    }
  }
}