def factors(n : Int) Stream[Int] = {
  def fact(n : Int, s : Stream[Int]) Stream[Int] = {
    if(n == 1)
      s
    else if(n % s.head == 0)
      s.head #:: fact(n/s.head,s.tail)
    else
      fact(n,s.tail)
  }
  fact(n, primes)
}
