object foldComparisons {
	val l = List(1,2,3,4)                     //> l  : List[Int] = List(1, 2, 3, 4)
	
	(l foldRight 0)((x, y) => x - y)          //> res0: Int = -2
	(l foldLeft  0)((x, y) => x - y)          //> res1: Int = -10
	
	(l foldRight 0)((x, y) => (x + y) / 2)    //> res2: Int = 1
	(l foldLeft  0)((x, y) => (x + y) / 2)    //> res3: Int = 3
	
	(l foldRight 0)((x, y) => x)              //> res4: Int = 1
	(l foldLeft  0)((x, y) => x)              //> res5: Int = 0
	
	(l foldRight 0)((x, y) => x+y)            //> res6: Int = 10
	(l foldLeft  0)((x, y) => x+y)            //> res7: Int = 10
}