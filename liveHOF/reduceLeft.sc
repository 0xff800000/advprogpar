object reduceLeft {
	val l = List(4, 1, 12, 7)                 //> l  : List[Int] = List(4, 1, 12, 7)

	l reduceLeft ((x: Int, y: Int) ⇒ x + y)   //> res0: Int = 24
	l reduceLeft ((x, y) ⇒ x min y)           //> res1: Int = 1
	l reduceLeft (_ max _)                    //> res2: Int = 12

	def verboseMax = (x: Int, y: Int) ⇒ {
		val winner = x max y
		println(s"compared $x to $y, $winner was larger")
		winner
	}                                         //> verboseMax: => (Int, Int) => Int

	l reduceLeft (verboseMax)                 //> compared 4 to 1, 4 was larger
                                                  //| compared 4 to 12, 12 was larger
                                                  //| compared 12 to 7, 12 was larger
                                                  //| res3: Int = 12
}