object filtering {
	val nums = List(5, 1, 4, 3, 2)            //> nums  : List[Int] = List(5, 1, 4, 3, 2)
	nums filter (_ > 2)                       //> res0: List[Int] = List(5, 4, 3)
	nums filter (_ > 2) map (_ + 1)           //> res1: List[Int] = List(6, 5, 4)

	val fruits = List("orange", "peach", "apple", "banana")
                                                  //> fruits  : List[String] = List(orange, peach, apple, banana)
	fruits filter (_.startsWith("a"))         //> res2: List[String] = List(apple)

}