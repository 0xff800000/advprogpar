object foldRight {
	val l = List(1, 2, 3, 4)

	def convert(x: Int) : String = {
		x match{
			case 1 => "one"
			case 2 => "two"
			case 3 => "three"
			case 4 => "four"
			case 5 => "five"
			case _ => "other"
		}
	}

	(l foldRight "")((elem,accum) => convert(elem) + "," + accum)
	(l foldLeft "")((accum,elem) => convert(elem) + "," + accum)


	(l foldRight 0)((x, y) => x + y)
	(l foldRight 1)((x, y) => x * y)

	(List(1, 2, 3) foldRight 2)((x,  y) => x - y)
	List("abc", "def", "ghi").foldRight("")((a, b) => a + b)
	List("abc", "def", "ghi").foldLeft("")((a, b) => a + b)
	List(1, 2, 3).foldRight(List(4))((e: Int, list: List[Int]) => e :: list)
}