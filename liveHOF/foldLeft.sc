object foldLeft {

	val l = List(1, 2, 3, 4)
	l.foldLeft(0)((y, x) ⇒ x + y)
	l.foldLeft(1)((y, x) ⇒ x * y)

	List(1, 2, 3).foldLeft(0)((x, y) ⇒ x - y)
	List(1, 2, 3).foldLeft(0)((y, x) ⇒ x - y)
	List(1, 2, 3).foldRight(0)((x, y) ⇒ x - y)
	List(1, 2, 3).foldRight(0)((y, x) ⇒ x - y)

	List("abc", "def", "ghi").foldLeft("")((a, b) ⇒ a + "," + b)

	List("abc", "def", "ghi").foldLeft("")((b, a) ⇒ a + b)
	List(1, 2, 3).foldLeft(List(4))((list, e) ⇒ e :: list)

	List(1, 2, 3).foldLeft(List[Int]())((theList, elem) ⇒ elem :: theList)
}