object hofScala {
	val n = 7                                 //> n  : Int = 7
	val r = List.range(1, n)                  //> r  : List[Int] = List(1, 2, 3, 4, 5, 6)

	def rep[A](l: A) = List[A](l, l)          //> rep: [A](l: A)List[A]

	def dup[A](l: List[A]): List[A] = {
		l.flatMap(rep)
	}                                         //> dup: [A](l: List[A])List[A]

	List(1, 2, 3).map(rep)                    //> res0: List[List[Int]] = List(List(1, 1), List(2, 2), List(3, 3))
	dup(List(1, 2, 3))                        //> res1: List[Int] = List(1, 1, 2, 2, 3, 3)

}