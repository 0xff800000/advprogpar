object reduceRight {
	val abc = List("A", "B", "C")             //> abc  : List[String] = List(A, B, C)

	def add(x: String, y: String) = {
		println(s"op: $x + $y= ${x + y}")
		x + y
	}                                         //> add: (x: String, y: String)String

	abc.reduceLeft(add)                       //> op: A + B= AB
                                                  //| op: AB + C= ABC
                                                  //| res0: String = ABC
	abc.reduceRight(add)                      //> op: B + C= BC
                                                  //| op: A + BC= ABC
                                                  //| res1: String = ABC


	// Second example, shows the type constraints
	val l = List(1,2,3,4)                     //> l  : List[Int] = List(1, 2, 3, 4)
	def concat1(x: Any, y: Int): Any = x + "-" + y
                                                  //> concat1: (x: Any, y: Int)Any
	def concat2(x: Int, y: Any): Any = x + "-" + y
                                                  //> concat2: (x: Int, y: Any)Any

	l reduceLeft (concat1)                    //> res2: Any = 1-2-3-4
	l reduceRight (concat2)                   //> res3: Any = 1-2-3-4

}