object map {
	val l = List(1, 2, 3, 4, 5)               //> l  : List[Int] = List(1, 2, 3, 4, 5)

	// Showing verbosity levels
	l map ((x: Int) => x * 2)                 //> res0: List[Int] = List(2, 4, 6, 8, 10)
	l map ((x) => x * 2)                      //> res1: List[Int] = List(2, 4, 6, 8, 10)
	l map (x => x * 2)                        //> res2: List[Int] = List(2, 4, 6, 8, 10)
	l map (_ * 2)                             //> res3: List[Int] = List(2, 4, 6, 8, 10)

	// Type changes
	l map ((x) => x * 2.0)                    //> res4: List[Double] = List(2.0, 4.0, 6.0, 8.0, 10.0)
	l map ((x: Int) => x.toString)            //> res5: List[String] = List(1, 2, 3, 4, 5)
	l map ((x: Int) => if (x % 2 == 0) true else false)
                                                  //> res6: List[Boolean] = List(false, true, false, true, false)
}